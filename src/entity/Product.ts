import { Column, Entity, PrimaryGeneratedColumn} from "typeorm"
@Entity()
export class Product {
    @PrimaryGeneratedColumn({name:"product_id"})
    id:number;

    @Column({name:"producy_name"})
    name:string;

    @Column({name:"product_price"})
    price: number;

}